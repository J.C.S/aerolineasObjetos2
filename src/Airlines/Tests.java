package Airlines;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

import Criterios.AmenazaTerrorista;
import Criterios.CriterioDeVuelo;
import Criterios.Laxa;
import Criterios.Segura;
import Politicas.Estricta;
import Politicas.Politica;
import Politicas.Remate;
import Politicas.VentaAnticipada;
import TipoDeVuelo.VueloCharter;
import TipoDeVuelo.VueloDeCarga;
import TipoDeVuelo.VueloGeneral;
import TipoDeVuelo.VueloPasajeros;

public class Tests {

	// 1
	@Test
	public void test() {
		Avion avion = new Avion("avion", 100, 3.30, 11200, 1022);
		VueloPasajeros vuelo001 = new VueloPasajeros("vuelo101", LocalDate.of(2017, Month.AUGUST, 12), "Argentina",
				"Espana", 13000);
		vuelo001.setAvion(avion);

		assertEquals(100, vuelo001.getCantidadDeAsientosLibres());
	}

	@Test
	public void pasajesVendidos() {
		Avion avion = new Avion("avion",100, 3.30, 11200, 1022);
		VueloPasajeros vuelo001 = new VueloPasajeros("vuelo101", LocalDate.of(2017, Month.APRIL, 02), "Argentina",
				"Alemania", 130000);
		vuelo001.setAvion(avion);

		Pasaje pasaje001 = new Pasaje(389281234, "Nahuel", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje002 = new Pasaje(389281234, "Juan", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje003 = new Pasaje(389281234, "Ivan", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje004 = new Pasaje(329481738, "pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		vuelo001.addPasaje(pasaje001);
		vuelo001.addPasaje(pasaje002);
		vuelo001.addPasaje(pasaje003);
		vuelo001.addPasaje(pasaje004);

		assertEquals(96, vuelo001.getCantidadDeAsientosLibres());
	}

	@Test
	public void pasajesVendidosDesdeEmpresa() {
		Avion avion = new Avion("avion",100, 3.30, 11200, 1022);
		VueloPasajeros vuelo001 = new VueloPasajeros("vuelo101", LocalDate.of(2017, Month.JUNE, 12), "Francia",
				"Espana", 120000);
		vuelo001.setAvion(avion);

		Pasaje pasaje001 = new Pasaje(389281234, "pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		Empresa americanAirlines = new Empresa();

		Politica estrict = new Estricta();
		CriterioDeVuelo seg = new Segura();

		vuelo001.setPolitica(estrict);
		americanAirlines.setCriterio(seg);
		americanAirlines.setVuelo(vuelo001);

		americanAirlines.venderPasaje(pasaje001, vuelo001);
		assertEquals(99, vuelo001.getCantidadDeAsientosLibres());
	}

	@Test
	public void pasajesVendidosAVueloCharter() {
		Avion avion = new Avion("avion",100, 3.30, 11200, 1022);
		VueloGeneral vuelo001 = new VueloCharter("vuelo101", LocalDate.of(2017, Month.JUNE, 12), "Francia", "Espana",
				120000, 50);

		vuelo001.setAvion(avion);

		Pasaje pasaje001 = new Pasaje(389281234, "pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		Empresa americanAirlines = new Empresa();

		Politica estrict = new Estricta();
		CriterioDeVuelo seg = new Segura();
		vuelo001.setPolitica(estrict);
		americanAirlines.setCriterio(seg);
		americanAirlines.setVuelo(vuelo001);

		americanAirlines.venderPasaje(pasaje001, vuelo001);

		assertEquals(24, vuelo001.getCantidadDeAsientosLibres());

	}

	@Test
	public void pasajesVendidosAVueloGarga() {
		Avion avion = new Avion("avion",100, 3.30, 11200, 1022);
		VueloGeneral vuelo001 = new VueloDeCarga("vuelo 010", LocalDate.of(2017, Month.APRIL, 20), "Argentina", "Japon",
				5900300);

		vuelo001.setAvion(avion);

		Pasaje pasaje001 = new Pasaje(389281234, "Nahuel", LocalDate.of(2016, Month.DECEMBER, 21));

		Pasaje pasaje002 = new Pasaje(389281234, "Juan", LocalDate.of(2016, Month.DECEMBER, 21));

		Pasaje pasaje003 = new Pasaje(389281234, "Ivan", LocalDate.of(2016, Month.DECEMBER, 21));

		Empresa americanAirlines = new Empresa();

		Politica rematon = new Remate();

		CriterioDeVuelo seg = new Segura();

		americanAirlines.setVuelo(vuelo001);
		americanAirlines.setCriterio(seg);

		vuelo001.setPolitica(rematon);

		americanAirlines.venderPasaje(pasaje001, vuelo001);
		americanAirlines.venderPasaje(pasaje002, vuelo001);
		americanAirlines.venderPasaje(pasaje003, vuelo001);

		assertEquals(27, vuelo001.getCantidadDeAsientosLibres());
	}

	// 2
	@Test
	public void EsUnVueloDePAsajerosRelajado() {
		Avion avion = new Avion("avion",90, 4.50, 12000, 1029);
		VueloGeneral vuelo002 = new VueloPasajeros("vuelo110", LocalDate.of(2016, Month.MAY, 18), "Espa�a", "Portugal",
				100);
		vuelo002.setAvion(avion);
		assertTrue(vuelo002.esRelajado());
	}

	@Test
	public void noesEsRelajadoVueloDePasajeros() {
		Avion avion = new Avion("avion", 100, 1.30, 12100, 1211);
		VueloGeneral vuelo002 = new VueloPasajeros("vuelo101", LocalDate.of(2017, Month.MARCH, 15), "Portual", "Brazil",
				300000);
		vuelo002.setAvion(avion);
		assertFalse(vuelo002.esRelajado());
	}

	@Test
	public void EsUnVueloCharterRelajado() {
		Avion avion = new Avion("avion", 70, 4.30, 12100, 1211);
		VueloGeneral vuelo002 = new VueloCharter("vuelo102", LocalDate.of(2017, Month.MARCH, 15), "Portual", "Brazil",
				300000, 30);
		vuelo002.setAvion(avion);
		assertTrue(vuelo002.esRelajado());
	}

	@Test
	public void NoEsUnVueloCharterRelajado() {
		Avion avion = new Avion("avion", 120, 2.30, 12100, 1211);
		VueloGeneral vuelo002 = new VueloCharter("vuelo102", LocalDate.of(2017, Month.MARCH, 15), "Portual", "Brazil",
				300000, 30);
		vuelo002.setAvion(avion);
		assertFalse(vuelo002.esRelajado());
	}

	@Test
	public void EsUnVueloDeCargaRelajado() {
		Avion avion = new Avion("avion", 70, 4.30, 12100, 1211);
		VueloGeneral vuelo002 = new VueloDeCarga("vuelo102", LocalDate.of(2017, Month.MARCH, 15), "Portual", "Brazil",
				300000);
		vuelo002.setAvion(avion);
		assertTrue(vuelo002.esRelajado());
	}

	@Test
	public void NoEsUnVueloDeCargaRelajado() {
		Avion avion = new Avion("avion", 120, 2.30, 12100, 1211);
		VueloGeneral vuelo002 = new VueloDeCarga("vuelo102", LocalDate.of(2017, Month.MARCH, 15), "Portual", "Brazil",
				300000);
		vuelo002.setAvion(avion);
		assertFalse(vuelo002.esRelajado());
	}

	// 3
	@Test
	public void noPuedeVenderAVueloConAmenenazaTerrorista() {
		Avion avion = new Avion("avion", 100, 1.30, 11011, 122);
		VueloPasajeros vuelo003 = new VueloPasajeros("vuelo100", LocalDate.of(2017, Month.MARCH, 16), "Brazil",
				"Estados Unidos", 13450);

		vuelo003.setAvion(avion);

		Empresa americanAirlines = new Empresa();
		americanAirlines.setVuelo(vuelo003);
		AmenazaTerrorista alertaRoja = new AmenazaTerrorista();
		americanAirlines.setCriterio(alertaRoja);
		assertFalse(americanAirlines.sePuedeVenderPasaje(vuelo003));
	}

	@Test
	public void sePuedeVenderAVueloSeguro() {
		Avion avion = new Avion("avion", 100, 1.30, 1000, 1200);
		VueloPasajeros vuelo004 = new VueloPasajeros("vuelo100", LocalDate.of(2017, Month.JANUARY, 12),
				"Estados Unidos", "Chile", 144444);
		vuelo004.setAvion(avion);
		Empresa americanAirlines = new Empresa();
		americanAirlines.setVuelo(vuelo004);
		CriterioDeVuelo seg = new Segura();
		americanAirlines.setCriterio(seg);

		assertTrue(americanAirlines.sePuedeVenderPasaje(vuelo004));
	}

	@Test
	public void NoSePuedeVenderAVueloSeguro() {
		Avion avion = new Avion("avion", 3, 1.30, 1000, 1200);
		VueloGeneral vuelo004 = new VueloPasajeros("vuelo100", LocalDate.of(2017, Month.JANUARY, 12), "Estados Unidos",
				"Chile", 144444);

		vuelo004.setAvion(avion);

		Empresa americanAirlines = new Empresa();

		americanAirlines.setVuelo(vuelo004);

		CriterioDeVuelo seg = new Segura();

		americanAirlines.setCriterio(seg);

		Pasaje pasaje001 = new Pasaje(389281234, "pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		assertTrue(americanAirlines.sePuedeVenderPasaje(vuelo004));
	}

	@Test
	public void sePuedeVenderAVueloPorPocentaje() {
		Avion avion = new Avion("avion", 100, 1.30, 1000, 1200);
		VueloPasajeros vuelo004 = new VueloPasajeros("vuelo100", LocalDate.of(2017, Month.JANUARY, 12),
				"Estados Unidos", "Chile", 144444);

		vuelo004.setAvion(avion);

		Empresa americanAirlines = new Empresa();
		americanAirlines.setVuelo(vuelo004);

		CriterioDeVuelo seg = new Segura();

		americanAirlines.setCriterio(seg);

		assertTrue(americanAirlines.sePuedeVenderPasaje(vuelo004));
	}

	@Test
	public void NoSePuedeVenderConLaxa() {
		Avion avion = new Avion("avion", 3, 4.50, 12000, 1029);
		VueloPasajeros vuelo002 = new VueloPasajeros("vuelo110", LocalDate.of(2017, Month.APRIL, 01), "Espana",
				"Portugal", 1922);
		vuelo002.setAvion(avion);

		Empresa american = new Empresa();
		Laxa laxa = new Laxa();
		american.setCriterio(laxa);
		american.setVuelo(vuelo002);

		Pasaje pasaje001 = new Pasaje(38928123, "Nahuel", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje002 = new Pasaje(38951524, "Juan", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje003 = new Pasaje(38245345, "Ivan", LocalDate.of(2016, Month.DECEMBER, 21));

		Pasaje pasaje004 = new Pasaje(38462246, "Fede", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje005 = new Pasaje(35029482, "Vale", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje006 = new Pasaje(38245653, "Nati", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje007 = new Pasaje(38936734, "Carlos", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje008 = new Pasaje(36734567, "Ben", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje009 = new Pasaje(36785356, "Chino", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje010 = new Pasaje(38928456, "Nahuel", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje011 = new Pasaje(38234564, "Ale", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje012 = new Pasaje(38234563, "Fede B", LocalDate.of(2016, Month.DECEMBER, 21));
		Pasaje pasaje013 = new Pasaje(36452345, "Fede A", LocalDate.of(2016, Month.DECEMBER, 21));

		vuelo002.addPasaje(pasaje001);
		vuelo002.addPasaje(pasaje002);
		vuelo002.addPasaje(pasaje003);
		vuelo002.addPasaje(pasaje004);
		vuelo002.addPasaje(pasaje005);
		vuelo002.addPasaje(pasaje006);
		vuelo002.addPasaje(pasaje007);
		vuelo002.addPasaje(pasaje008);
		vuelo002.addPasaje(pasaje009);
		vuelo002.addPasaje(pasaje010);
		vuelo002.addPasaje(pasaje011);
		vuelo002.addPasaje(pasaje012);
		vuelo002.addPasaje(pasaje013);

		assertEquals(new HashSet<>(Arrays.asList(vuelo002)), american.getVuelos());

		assertEquals(13, vuelo002.getAsientosOcupados());

		assertFalse(american.sePuedeVenderPasaje(vuelo002));
	}

	// 4
	@Test
	public void precioDeVentaDeVentaEstricta() {
		Avion boing474 = new Avion("boing 474", 120, 3, 10000, 1211);
		VueloPasajeros vuelo005 = new VueloPasajeros("vuelo110", LocalDate.of(2017, Month.MAY, 17), "Chile", "Canada",
				192223);

		Estricta estrict = new Estricta();
		vuelo005.setAvion(boing474);

		vuelo005.setPrecioEstandar(3.2);
		vuelo005.setPolitica(estrict);

		assertNotEquals(2.2, vuelo005.getPrecioDeVenta(), 0.000001);
	}

	@Test
	public void precioDeVentaAnticipada() {
		Avion boing = new Avion("avion", 100, 312, 10111, 141);
		VueloPasajeros vuelo006 = new VueloPasajeros("vuelo111", LocalDate.of(2017, Month.MARCH, 12), "Canada",
				"Alaska", 120);
		Politica ventaAnti = new VentaAnticipada();
		Empresa empresaAviones = new Empresa();

		vuelo006.setPolitica(ventaAnti);
		vuelo006.setAvion(boing);
		empresaAviones.setVuelo(vuelo006);

		vuelo006.setPrecioEstandar(100.0);

		assertEquals(70.0, vuelo006.getPrecioDeVenta(), 0.0001);
	}

	// 5
	@Test
	public void venderPasajeEstricto() {
		Avion boing478 = new Avion("Boing 478", 120, 3, 1000, 1311);
		VueloPasajeros vuelo007 = new VueloPasajeros("vuelo009", LocalDate.of(2017, Month.OCTOBER, 12), "Alaska",
				"India", 109000);
		Politica estict = new Estricta();
		CriterioDeVuelo seguro = new Segura();
		Empresa AmericanAirlines = new Empresa();

		Pasaje pasaje001 = new Pasaje(389281234, "pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		vuelo007.setAvion(boing478);
		vuelo007.setPrecioEstandar(100.0);
		vuelo007.setPolitica(estict);

		AmericanAirlines.setCriterio(seguro);
		AmericanAirlines.setVuelo(vuelo007);

		AmericanAirlines.venderPasaje(pasaje001, vuelo007);

		assertEquals(1, vuelo007.getAsientosOcupados());
		assertTrue(vuelo007.getPasajes().contains(pasaje001));
	}

	@Test
	public void venderPasajeConVentaAnticipada() {
		Avion avion = new Avion("avion", 50, 4, 1200, 3200);
		VueloPasajeros vuelo007 = new VueloPasajeros("vuelo009", LocalDate.of(2018, Month.JUNE, 11), "Alaska", "India",
				1900000);
		VentaAnticipada ventaanti = new VentaAnticipada();
		CriterioDeVuelo seguro = new Segura();
		Empresa AmericanAirlines = new Empresa();

	}

	// 6
	@Test
	public void totalGanadoDeTodosLosPasajesVendidos() {
		Avion boing478 = new Avion("Boing 478", 120, 3, 1000, 1311);
		VueloPasajeros vuelo007 = new VueloPasajeros("vuelo009", LocalDate.of(2017, Month.OCTOBER, 12), "Alaska",
				"India", 109000);
		Politica estict = new Estricta();
		CriterioDeVuelo seguro = new Segura();
		Empresa AmericanAirlines = new Empresa();

		Pasaje pasaje001 = new Pasaje(389281234, "Pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		vuelo007.setAvion(boing478);
		vuelo007.setPrecioEstandar(100.0);
		vuelo007.setPolitica(estict);

		AmericanAirlines.setCriterio(seguro);
		AmericanAirlines.setVuelo(vuelo007);

		AmericanAirlines.venderPasaje(pasaje001, vuelo007);

		assertEquals(100.0, vuelo007.getImporteTotalJuntado(), 0.00001);
	}

	// 7
	@Test
	public void pasoDelAvionDePasajeros() {
		Avion boing478 = new Avion("Boing 478", 120, 3, 1000, 50);
		VueloPasajeros vuelo007 = new VueloPasajeros("vuelo009", LocalDate.of(2017, Month.OCTOBER, 12), "Alaska",
				"India", 109);

		Iata iata = new Iata();

		iata.setEquipamientoReglamentario(20.0);
		iata.setPesoEstablecido(10.0);

		vuelo007.setPesoPermitidoEnEquipaje(10.0);

		Politica estict = new Estricta();
		CriterioDeVuelo seguro = new Segura();

		Empresa AmericanAirlines = new Empresa();

		Pasaje pasaje001 = new Pasaje(389281234, "pepe", LocalDate.of(2017, Month.DECEMBER, 21));

		vuelo007.setAvion(boing478);
		vuelo007.setPrecioEstandar(100.0);
		vuelo007.setPolitica(estict);

		AmericanAirlines.setCriterio(seguro);
		AmericanAirlines.setVuelo(vuelo007);

		AmericanAirlines.venderPasaje(pasaje001, vuelo007);

		assertEquals(6490.0, vuelo007.pesoMaximoDelVuelo(), 0.001);
	}

	// 8
	@Test
	public void fechaDeViajeDePasajero() {
		VueloStore vuelo = new VueloStore();

		vuelo.iniciarVuelo();
		assertEquals(LocalDate.of(2017, Month.DECEMBER, 20), vuelo.fechaDeVuelo(39092345, "Japon"));
	}

}
