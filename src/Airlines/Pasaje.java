package Airlines;

import java.time.LocalDate;
import java.time.Month;

import org.uqbar.commons.utils.Observable;

@Observable
public class Pasaje {

	private int dni;
	private LocalDate fechaDeVenta;
	private double precioComprado;
	private String nombre;

	public Pasaje(int dni, String name, LocalDate date) {
		this.dni = dni;
		this.nombre = name;
		this.fechaDeVenta = date;
	}

	public Pasaje() {
	
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecioComprado() {
		return precioComprado;
	}

	public void setPrecioComprado(double precioVentido) {
		this.precioComprado = precioVentido;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getDni() {
		return dni;
	}

	public void setFechaDeVenta(LocalDate fechaDeVenta) {
		this.fechaDeVenta = fechaDeVenta;
	}

	public LocalDate getFechaDeVenta() {
		return fechaDeVenta;
	}

}
