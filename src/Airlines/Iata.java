package Airlines;

import org.uqbar.commons.utils.Observable;

@Observable
public class Iata {

	private static Iata store = new Iata();

	private double pesoEstablecido;
	private double pesoDeEquipamientoReglamentario;

	public static Iata store(){
		return store;
	}
	
	public Iata(){
	}

	public double getPesoEstablecido() {
		return pesoEstablecido;
	}

	public void setPesoEstablecido(double pesoEstablecido) {
		this.pesoEstablecido = pesoEstablecido;
	}

	public void setEquipamientoReglamentario(Double kg) {
		this.pesoDeEquipamientoReglamentario = kg;
	}

	public double getPesoDeEquipamientoReglamentario() {
		return pesoDeEquipamientoReglamentario;
	}

}
