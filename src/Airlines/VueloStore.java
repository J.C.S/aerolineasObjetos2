package Airlines;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

import Criterios.CriterioDeVuelo;
import Criterios.Segura;
import Politicas.Estricta;
import Politicas.Politica;
import TipoDeVuelo.VueloCharter;
import TipoDeVuelo.VueloDeCarga;
import TipoDeVuelo.VueloGeneral;
import TipoDeVuelo.VueloPasajeros;

@Observable
public class VueloStore {

	private static VueloStore store = new VueloStore();

	private List<VueloGeneral> vuelos;

	public void setVuelos(List<VueloGeneral> vuelos) {
		this.vuelos = vuelos;
	}

	public VueloStore() {
		this.vuelos = new ArrayList<>();
	}

	public List<VueloGeneral> getVuelos() {
		return vuelos;
	}

	public static VueloStore store() {
		return store;
	}
	
	public void agregarVuelo(VueloGeneral vuelo){
		vuelos.add(vuelo);
	}

	public void iniciarVuelo() {
		Avion boing778 = new Avion("Boing 778", 4, 3, 1000, 123);
		Avion boing747 = new Avion("Boing 747", 150, 4, 15500, 300);
		Avion boing404 = new Avion("Boing 404", 120, 4, 1300, 200);
		Avion boing500 = new Avion("Boing 500",155, 3, 1200, 130);
		
		VueloGeneral vuelo001 = new VueloPasajeros("vuelo008", LocalDate.of(2017,Month.DECEMBER,20), "India", "Japon", 1000);
		VueloGeneral vuelo002 = new VueloPasajeros("vuelo001", LocalDate.of(2018, Month.JANUARY, 22), "Argentina", "Inglaterra", 12000);
		VueloGeneral vuelo003 = new VueloDeCarga("vuelo001c", LocalDate.of(2017, Month.DECEMBER, 30), "Argentina", "Brazil", 3000);
		VueloGeneral vuelo004 = new VueloCharter("vuelo004", LocalDate.of(2018, Month.JUNE, 27), "Argentina", "Japon", 20000, 20);
		Politica estrict = new Estricta();
		CriterioDeVuelo seguro = new Segura();

		
		Pasaje pasaje002 = new Pasaje(39352345, "Pepe peron", LocalDate.of(2017, Month.APRIL, 21));
		Pasaje pasaje003 = new Pasaje(38951524, "Juan Cruz Santamaria", LocalDate.of(2017, Month.APRIL, 21));
		Pasaje pasaje004 = new Pasaje(30683345, "Nahuel Martinez", LocalDate.of(2017, Month.APRIL, 21));
		Pasaje pasaje005 = new Pasaje(39092345, "Ivan Kerney", LocalDate.of(2017, Month.APRIL, 21));
		Pasaje pasaje006 = new Pasaje(38194828, "Federico Ferreyra", LocalDate.of(2017, Month.APRIL, 21));

		Empresa aerolines = new Empresa();
		
		vuelo001.setPrecioEstandar(100.0);
		vuelo002.setPrecioEstandar(160.0);
		vuelo003.setPrecioEstandar(50.0);
		vuelo004.setPrecioEstandar(180.5);
		
		vuelo001.setAvion(boing778);
		vuelo002.setAvion(boing747);
		vuelo003.setAvion(boing404);
		vuelo004.setAvion(boing500);
		
		aerolines.setVuelo(vuelo001);
		aerolines.setVuelo(vuelo002);
		aerolines.setVuelo(vuelo003);
		aerolines.setVuelo(vuelo004);
		
		vuelo001.setPolitica(estrict);
		vuelo002.setPolitica(estrict);
		vuelo003.setPolitica(estrict);
		vuelo004.setPolitica(estrict);
		
		aerolines.setCriterio(seguro);
		
		aerolines.setCriterioEnVuelo(vuelo001);
		aerolines.setCriterioEnVuelo(vuelo002);
		aerolines.setCriterioEnVuelo(vuelo003);
		aerolines.setCriterioEnVuelo(vuelo004);
		
		aerolines.venderPasaje(pasaje002, vuelo001);
		aerolines.venderPasaje(pasaje003, vuelo001);
		aerolines.venderPasaje(pasaje004, vuelo002);
		aerolines.venderPasaje(pasaje005, vuelo003);
		aerolines.venderPasaje(pasaje006, vuelo002);
		aerolines.venderPasaje(pasaje003, vuelo004);
		
		this.agregarVuelo(vuelo001);
		this.agregarVuelo(vuelo002);
		this.agregarVuelo(vuelo003);
		this.agregarVuelo(vuelo004);
	}
	
	public List<VueloGeneral> destinoDelVuelo(String destino){
		return store.vuelos.stream().filter(v -> v.getDestino().equals(destino)).collect(Collectors.toList());
	}
	
	public VueloGeneral vueloDePersonaBuscada(int dni, String destiny){
		return this.destinoDelVuelo(destiny).stream().filter(v -> v.estaPasaje(dni)).findAny().get();
	}
	
	public LocalDate fechaDeVuelo(int dni, String destiny){
		return this.vueloDePersonaBuscada(dni, destiny).getFechaDeVuelo();
	}
}
