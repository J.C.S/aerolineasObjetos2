
package Airlines;

import java.util.Collection;
import java.util.HashSet;

import org.uqbar.commons.utils.Observable;

import Criterios.CriterioDeVuelo;
import TipoDeVuelo.VueloGeneral;
import TipoDeVuelo.VueloPasajeros;

@Observable
public class Empresa {

	private CriterioDeVuelo criterio;
	private Collection<VueloGeneral> vuelos = new HashSet<>();

	public void setCriterio(CriterioDeVuelo criterio) {
		this.criterio = criterio;
	}

	public CriterioDeVuelo getCriterio() {
		return criterio;
	}

	private boolean seCumpleCriterio(VueloGeneral vuelo001) {
		return this.criterio.puedeVender(vuelo001);
	}

	public void setVuelos(Collection<VueloGeneral> vuelo) {
		this.vuelos = vuelo;
	}

	public void setVuelo(VueloGeneral vuelo) {
		this.vuelos.add(vuelo);
		vuelo.setEmpresa(this);
	}
	
	public Collection<VueloGeneral> getVuelos() {
		return vuelos;
	}

	public boolean vueloEstaEnEmpresa(VueloGeneral vuelo001) {
		return this.vuelos.contains(vuelo001);
	}

	public void venderPasaje(Pasaje pasaje, VueloGeneral vuelo001) {
		if (this.sePuedeVenderPasaje(vuelo001)) {
			vuelo001.addPasaje(pasaje);
			pasaje.setPrecioComprado(vuelo001.getPrecioDeVenta());
		} else {
			throw new RuntimeException("No se ha podido vender el pasaje");
		}
	}

	public void setPesoPermidoDeEquipajeEnVuelo(VueloPasajeros vuelo, double peso) {
		vuelo.setPesoPermitidoEnEquipaje(peso);
	}

	public boolean sePuedeVenderPasaje(VueloGeneral vuelo001) {
		return vueloEstaEnEmpresa(vuelo001) && seCumpleCriterio(vuelo001);
	}
	
	public void setCriterioEnVuelo(VueloGeneral vuelo){
		vuelo.setCriterio(this.criterio);
	}

}
