package Airlines;

import org.uqbar.commons.utils.Observable;

@Observable
public class Avion {

	private String nombre;
	private int cantidadDeAsientos;
	private double alturaCabina;
	private double pesoDelAvion;
	private double consumoDeNaftaPorKm;

	public Avion(String name, int cantidadDeAsientos, double cabina, double peso, double consumo) {
		super();
		this.nombre = name;
		this.cantidadDeAsientos = cantidadDeAsientos;
		this.alturaCabina = cabina;
		this.setPesoDelAvion(peso);
		this.consumoDeNaftaPorKm = consumo;
	}

	public void setCantidadDeAsientos(int cantidadDeAsientos) {
		this.cantidadDeAsientos = cantidadDeAsientos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidadDeAsientos() {
		return this.cantidadDeAsientos;
	}

	public double getAlturaCabina() {
		return alturaCabina;
	}

	public void setAlturaCabina(double alturaCabina) {
		this.alturaCabina = alturaCabina;
	}

	public double getPesoDelAvion() {
		return pesoDelAvion;
	}

	public void setPesoDelAvion(double pesoDelAvion) {
		this.pesoDelAvion = pesoDelAvion;
	}

	public void setConsumoDeNaftaPorKm(double consumoDeNaftaPorKm) {
		this.consumoDeNaftaPorKm = consumoDeNaftaPorKm;
	}

	public double getConsumoDeNaftaPorKm() {
		return consumoDeNaftaPorKm;
	}
}
