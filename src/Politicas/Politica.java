package Politicas;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public abstract  class Politica {

	public abstract double precioDeVenta (VueloGeneral vuelo);
}
