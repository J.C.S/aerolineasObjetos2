package Politicas;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class Estricta extends Politica {

	@Override
	public double precioDeVenta(VueloGeneral vuelo) {
		return vuelo.getPrecioEstandar();
	}
	

}
