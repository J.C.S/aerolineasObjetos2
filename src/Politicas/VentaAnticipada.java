package Politicas;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class VentaAnticipada extends Politica {

	@Override
	public double precioDeVenta(VueloGeneral vuelo) {
		if (vuelo.getAsientosOcupados()  < 40){
			return vuelo.getPrecioEstandar() - vuelo.getPrecioEstandar() * 0.30;
		}else if (vuelo.getAsientosOcupados() > 40 && vuelo.getAsientosOcupados() < 79){
			return vuelo.getPrecioEstandar() - vuelo.getPrecioEstandar() * 0.60;
		} else {
			return vuelo.getPrecioEstandar();
		}
	}
	

}
