package Politicas;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class Remate extends Politica {

	@Override
	public double precioDeVenta(VueloGeneral vuelo) {
		if (vuelo.getCantidadDeAsientosLibres() > 30){
			return vuelo.getPrecioEstandar() - vuelo.getPrecioEstandar() * 0.75;
		} else{
			return vuelo.getPrecioEstandar() - vuelo.getPrecioEstandar() * 0.50;
		}
	}

}
