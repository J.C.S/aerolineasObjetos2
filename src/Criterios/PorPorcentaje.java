 package Criterios;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class PorPorcentaje extends CriterioDeVuelo {

	@Override
	public boolean puedeVender(VueloGeneral vuelo) {
	return ((vuelo.getCantidadDeAsientosLibres() * 1.01 > vuelo.getCantidadDeAsientosLibres() ));
	}

}
