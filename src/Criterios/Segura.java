package Criterios;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class Segura extends CriterioDeVuelo {

	@Override
	public boolean puedeVender(VueloGeneral vuelo) {
		return vuelo.getCantidadDeAsientosLibres() >= 3;

	}
}
