package Criterios;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class Laxa extends CriterioDeVuelo {

	@Override
	public boolean puedeVender(VueloGeneral vuelo) {
		return (vuelo.getAsientosOcupados() < vuelo.getCantidadDeAsientosLibres() + 10);
	}

}
