package Criterios;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public class AmenazaTerrorista extends CriterioDeVuelo{

	@Override
	public boolean puedeVender(VueloGeneral vuelo) {
		return false;
	}

}
