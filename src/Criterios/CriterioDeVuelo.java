package Criterios;

import org.uqbar.commons.utils.Observable;

import TipoDeVuelo.VueloGeneral;

@Observable
public abstract class CriterioDeVuelo  {

	public abstract boolean puedeVender (VueloGeneral vuelo);
}
