package TipoDeVuelo;

import java.time.LocalDate;

import org.uqbar.commons.utils.Observable;

@Observable
public class VueloPasajeros extends VueloGeneral {

	private double pesoPermitidoEnEquipaje;

	public VueloPasajeros(String nombre, LocalDate fechaDeVuelo, String origen, String destino,
			double kilometrosARecorrer) {
		super(nombre, fechaDeVuelo, origen, destino, kilometrosARecorrer);
	}

	public double getPesoPermitidoEnEquipaje() {
		return pesoPermitidoEnEquipaje;
	}

	public void setPesoPermitidoEnEquipaje(double pesoPermitidoEnEquipaje) {
		this.pesoPermitidoEnEquipaje = pesoPermitidoEnEquipaje;
	}

	public double pesoPasajerosPorIata() {
		return this.getAsientosOcupados() * this.getIata().getPesoEstablecido();
	}

	public double importeTotalPasajesVendido() {
		return this.pasajes.stream().mapToDouble(p -> p.getPrecioComprado()).sum();
	}

	@Override
	public double pesoDeCarga() {
		return this.getAsientosOcupados() * this.pesoPermitidoEnEquipaje;
	}

	public double getImporteTotalJuntado() {
		return this.pasajes.stream().mapToDouble(p -> p.getPrecioComprado()).sum();
	}

	@Override
	public int getCantidadDeAsientosEnTotal() {
		return this.avion.getCantidadDeAsientos();
	}

	@Override
	public void setTipo() {
		this.tipo = "normal";
	}

}
