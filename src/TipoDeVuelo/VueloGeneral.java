package TipoDeVuelo;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;

import org.uqbar.commons.utils.Observable;

import Airlines.Avion;
import Airlines.Empresa;
import Airlines.Iata;
import Airlines.Pasaje;
import Criterios.CriterioDeVuelo;
import Politicas.Politica;

@Observable
public abstract class VueloGeneral {

	protected Avion avion;
	protected String nombre;
	protected static Iata iata;
	protected Collection<Pasaje> pasajes = new HashSet<>();
	protected LocalDate fechaDeVuelo;
	protected String origen;
	protected String destino;
	protected double precioEstandar;
	protected Politica politica;
	protected CriterioDeVuelo criterio;
	protected double kilometrosARecorrer;
	protected Empresa empresa;
	

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	protected String tipo;
	
	public VueloGeneral(String nombre, LocalDate fechaDeVuelo, String origen, String destino,
			double kilometrosARecorrer) {
		this.nombre = nombre;
		this.fechaDeVuelo = fechaDeVuelo;
		this.origen = origen;
		this.destino = destino;
		this.kilometrosARecorrer = kilometrosARecorrer;
		this.setTipo();
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}


	public Iata getIata(){
		return iata;
	}

	public abstract double pesoPasajerosPorIata();

	public void addPasaje(Pasaje pasaje) {
		pasajes.add(pasaje);
	}

	public void addPasajes(Collection<Pasaje> allPasajes) {
		pasajes = allPasajes;
	}

	public Collection<Pasaje> getPasajes() {
		return this.pasajes;
	}

	public boolean estaPasaje(int dni) {
		return this.pasajes.stream().anyMatch(p -> p.getDni() == dni);
	}

	public LocalDate getFechaDeVuelo() {
		return fechaDeVuelo;
	}

	public void setFechaDeVuelo(LocalDate fechaDeVuelo) {
		this.fechaDeVuelo = fechaDeVuelo;
	}

	public int getAsientosOcupados() {
		return this.pasajes.size();
	}

	public int getCantidadDeAsientosLibres() {
		return this.getCantidadDeAsientosEnTotal() - this.getAsientosOcupados();
	}

	public abstract int getCantidadDeAsientosEnTotal();

	public String getOrigen() {
		return origen;
	}

	public String getDestino() {
		return destino;
	}

	public void setPrecioEstandar(double precioEstandar) {
		this.precioEstandar = precioEstandar;
	}

	public double getPrecioEstandar() {
		return this.precioEstandar;
	}

	public double getPrecioDeVenta() {
		return this.politica.precioDeVenta(this);
	}

	public Politica getPolitica() {
		return politica;
	}

	public void setPolitica(Politica politica) {
		this.politica = politica;
	}

	public abstract double pesoDeCarga();

	public void setKilometrosArecorrer(double km) {
		this.kilometrosARecorrer = km;
	}

	public void getKilometrosArecorrer(Double km) {
		this.setKilometrosArecorrer(km);
	}

	public double pesoMaximoDelVuelo() {
		return this.avion.getPesoDelAvion() + this.pesoPasajerosPorIata() + this.pesoDeCarga() + this.getPesoDeLaNafta()
				+ VueloGeneral.iata.getPesoDeEquipamientoReglamentario();
	}

	public double getPesoDeLaNafta() {
		return this.kilometrosARecorrer * this.avion.getConsumoDeNaftaPorKm();
	}
	
	public boolean esRelajado() {
		return ((avion.getCantidadDeAsientos() < 100) && (avion.getAlturaCabina() > 4));
	}

	public double getKilometrosARecorrer() {
		return kilometrosARecorrer;
	}

	public void setKilometrosARecorrer(double kilometrosARecorrer) {
		this.kilometrosARecorrer = kilometrosARecorrer;
	}

	public Avion getAvion() {
		return avion;
	}

	public void setPasajes(Collection<Pasaje> pasajes) {
		this.pasajes = pasajes;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getTipo() {
		return tipo;
	}

	public abstract void setTipo();
	
	public CriterioDeVuelo getCriterio() {
		return criterio;
	}

	public void setCriterio(CriterioDeVuelo criterio) {
		this.criterio = criterio;
	}
	
	public boolean estaALaVenta(){
		return this.criterio.puedeVender(this);	
	}
	
	public void venderPasaje(Pasaje pasaje, VueloGeneral vuelo001) {
		if (this.getEmpresa().sePuedeVenderPasaje(vuelo001)) {
			vuelo001.addPasaje(pasaje);
			pasaje.setPrecioComprado(vuelo001.getPrecioDeVenta());
		} else {
			throw new RuntimeException("No se ha podido vender el pasaje");
		}
	}
}