package TipoDeVuelo;

import java.time.LocalDate;

import org.uqbar.commons.utils.Observable;

@Observable
public class VueloDeCarga extends VueloGeneral {
	public double equipamientoDeSeguridad;
	public double pesoDeCarga;

	public VueloDeCarga(String nombre, LocalDate fechaDeVuelo, String origen, String destino,
			double kilometrosARecorrer) {
		super(nombre, fechaDeVuelo, origen, destino, kilometrosARecorrer);
	}

	public double getEquipamientoDeSeguridad() {
		return equipamientoDeSeguridad;
	}

	public void setEquipamientoDeSeguridad(double equipamientoDeSeguridad) {
		this.equipamientoDeSeguridad = equipamientoDeSeguridad;
	}

	public double getPesoDeCarga() {
		return pesoDeCarga;
	}

	public void setPesoDeCarga(double pesoDeCarga) {
		this.pesoDeCarga = pesoDeCarga;
	}

	@Override
	public double pesoDeCarga() {
		return this.pesoDeCarga + this.equipamientoDeSeguridad;
	}

	@Override
	public double pesoPasajerosPorIata() {
		return 0;
	}

	@Override
	public int getCantidadDeAsientosEnTotal() {
		return 30;
	}

	@Override
	public void setTipo() {
		this.tipo = "Carga";
	}

}
