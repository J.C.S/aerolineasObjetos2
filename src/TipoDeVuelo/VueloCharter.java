package TipoDeVuelo;

import java.time.LocalDate;

import org.uqbar.commons.utils.Observable;

@Observable
public class VueloCharter extends VueloGeneral {

	private int pasajerosAsignados;
	
	public VueloCharter(String nombre, LocalDate fechaDeVuelo, String origen, String destino,
			double kilometrosARecorrer, int pasajerosAsignadosA) {
		super(nombre, fechaDeVuelo, origen, destino, kilometrosARecorrer);
		this.pasajerosAsignados = pasajerosAsignadosA;
	}

	@Override
	public double pesoDeCarga() {
		return 5000;
	}

	@Override
	public double pesoPasajerosPorIata() {
		return this.getCantidadDeAsientosEnTotal() * VueloGeneral.iata.getPesoEstablecido();
	}


	@Override
	public int getCantidadDeAsientosEnTotal() {
		return (this.avion.getCantidadDeAsientos()-this.pasajerosAsignados)-25;
	}

	public int getPasajerosAsignados() {
		return pasajerosAsignados;
	}

	public void setPasajerosAsignados(int pasajerosAsignados) {
		this.pasajerosAsignados = pasajerosAsignados;
	}

	@Override
	public void setTipo() {
		this.tipo = "Charter";
	}

}
